class Person {
  String name;
  String img;
  bool smoker;
  double drivingSpeed;

  Person(this.name, this.img, this.drivingSpeed, {this.smoker: false});
}
