import 'person.dart';

class Car {
  String model;
  String img;

  Person owner;
  int seats;
  List<Person> passengers;

  Car(this.model, this.img, this.seats, this.owner)
      : passengers = List(seats)..[0] = owner;
}