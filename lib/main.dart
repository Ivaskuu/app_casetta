import 'package:flutter/material.dart';

import 'models/car.dart';
import 'models/person.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'App Casetta',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: CarsPage(),
    );
  }
}

Person adrian = Person('Adrian', 'res/me.png', 0.85, smoker: true);
Person fabio = Person('Fabio', 'res/fabio.jpg', 0.35);
Person baio = Person('Baio', 'res/baio.jpg', 0.9);

class CarsPage extends StatefulWidget {
  List<Car> cars = [
    Car('BMW 120d', 'res/bmw.png', 4, adrian),
    Car('Renault Clio', 'res/clio.png', 5, fabio),
    Car('Volkswagen Polo', 'res/polo.png', 5, baio),
  ];

  @override
  _CarsPageState createState() => _CarsPageState();
}

class _CarsPageState extends State<CarsPage> {
  int actualCar = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        body: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0.0,
              title: Text(widget.cars[actualCar].model),
              centerTitle: true,
              leading: IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.arrow_back, color: Colors.white)),
            ),
            SizedBox.fromSize(
              size: Size.fromHeight(328.0),
              child: PageView.builder(
                onPageChanged: (newCar) => setState(() => actualCar = newCar),
                scrollDirection: Axis.horizontal,
                itemBuilder: (_, int i) => carDetails(widget.cars[i]),
                itemCount: widget.cars.length,
              ),
            ),
            LinearProgressIndicator(
                value: widget.cars[actualCar].owner.drivingSpeed,
                valueColor:
                    ColorTween(begin: Color(0xFF3F5EFB), end: Color(0xFFFC466B))
                        .animate(AlwaysStoppedAnimation(
                            widget.cars[actualCar].owner.drivingSpeed)),
                backgroundColor: Colors.black),
            Expanded(
              child: Material(
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 24.0),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: widget.cars[actualCar].passengers
                              .map((passenger) => passenger == null &&
                                      passenger ==
                                          widget.cars[actualCar].passengers.last
                                  ? IconButton(
                                      onPressed: () {},
                                      color: Colors.blue,
                                      icon:
                                          Icon(Icons.add, color: Colors.black))
                                  : personImg(passenger))
                              .toList()),
                    ),
                    Divider(),
                    Expanded(
                      child: ListView(padding: EdgeInsets.all(0.0), children: <
                          Widget>[
                            songTile('Sfera Ebbasta'),
                            songTile('DrefGold'),
                            songTile('Capo Plaza'),
                      ]),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }

  Stack carDetails(Car car) {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Image.asset(car.img),
      ],
    );
  }

  Widget personImg(Person person) {
    if (person != null) {
      return Stack(
        alignment: Alignment.bottomRight,
        children: <Widget>[
          CircleAvatar(
              backgroundColor: Colors.amber,
              child: CircleAvatar(backgroundImage: AssetImage(person.img))),
          person.smoker
              ? Align(
                  alignment: Alignment.bottomRight,
                  child: CircleAvatar(
                      radius: 8.0,
                      backgroundColor: Colors.red,
                      child: Icon(Icons.smoking_rooms,
                          color: Colors.white, size: 10.0)),
                )
              : Container()
        ],
      );
    } else {
      return CircleAvatar(
          backgroundColor: Colors.grey[300],
          child: Icon(Icons.account_circle, color: Colors.black));
    }
  }

  Widget songTile(String name) {
    return Container(
        margin: EdgeInsets.only(left: 16.0, right: 16.0, top: 7.0, bottom: 7.0),
      child: Material(
        elevation: 30.0,
        shadowColor: Colors.black54,
        color: Colors.white,
        borderRadius: BorderRadius.circular(6.0),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 6.0),
          child: ListTile(
            //leading: Image.asset('res/${plant.plantImg}'),
            title: Text(name,
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w700,
                    fontSize: 20.0)),
            trailing: IconButton(
              onPressed: () {},
              icon: Icon(Icons.delete, color: Colors.black26),
            ),
          ),
        ),
      ),
    );
  }
}
